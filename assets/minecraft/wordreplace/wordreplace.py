alpha = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz𐑐𐑑𐑒𐑓𐑔𐑕𐑖𐑗𐑘𐑙𐑚𐑛𐑜𐑝𐑞𐑟𐑠𐑡𐑢𐑣𐑤𐑥𐑦𐑧𐑨𐑩𐑪𐑫𐑬𐑭𐑮𐑯𐑰𐑱𐑲𐑳𐑴𐑵𐑶𐑷𐑸𐑹𐑺𐑻𐑼𐑽𐑾𐑿"
alphanumeric = alpha + "0123456789'-·"

missingWords = []

def replaceWords(text):
	words = []
	word = ""
	for char in text:
		if char in alphanumeric:
			word += char
		else:
			if word:
				words.append(word)
			words.append(char)
			word = ""
	if word:
		words.append(word)
	
	for i in range(len(words)):
		word = words[i].lower()
		if word in dontReplace:
			continue
		elif word in wordList:
			words[i] = "|".join(wordList[word])
		else:
			hasAlpha = False
			for char in word:
				if char in alpha:
					hasAlpha = True
					break

			if hasAlpha and not word in missingWords:
				missingWords.append(word)

	return words

#Read wordlists
print("Reading wordlists...", end="", flush=True)
with open("wordlists/kingsleyreadlexicon.tsv") as f:
	readLexLines = f.readlines()
with open("wordlists/mclist.tsv") as f:
	mcListLines = f.readlines()
lists = readLexLines + mcListLines
wordList = {}
for line in lists:
	if line == "\n":
		continue
	split = line.strip("\n").split("\t")
	key = split[0]
	if wordList.get(key) is None:
		wordList[key] = [split[1]]
	elif not split[1] in wordList[key]:
		wordList[key].append(split[1])

with open("wordlists/dontreplace.tsv") as f:
	dontReplaceLines = f.readlines()
dontReplace = []
for line in dontReplaceLines:
	if line == "\n":
		continue
	dontReplace.append(line.strip("\n"))

print("done.")

#Replace words in en_us.json (Vanilla Minecraft)
print("Replacing words in vanilla langfile...", end="", flush=True)
with open("latin/en_us.json") as f:
	latinLines = f.readlines()
shavianLines = []
for line in latinLines:
	if line in ["{\n", "}\n"]:
		shavianLines.append(line)
		continue

	key = line.split(":", 1)[0][3:-1]

	if line[-2] == "\"": #The second-to-last line doesn't have a comma at the end, so it needs a special case
		text = line.split(":", 1)[1][2:-2].replace("\\n", "\n")
	else:
		text = line.split(":", 1)[1][2:-3].replace("\\n", "\n")

	presets = {
		"language.name": "·𐑦𐑙𐑜𐑤𐑦𐑖",
		"language.region": "·𐑖𐑱𐑝𐑾𐑯",
		"language.code": "en_sh"
	}

	if key in presets:
		newLine = presets[key]
	else:
		newLine = "".join(replaceWords(text)).replace("\n", "\\n")

	if line[-2] == "\"":
		shavianLines.append(f"  \"{key}\": \"{newLine}\"\n")
	else:
		shavianLines.append(f"  \"{key}\": \"{newLine}\",\n")
print("done.")

print("Writing to shavian/new.json...", end="", flush=True)
with open("shavian/new.json", "w") as f:
	f.writelines(shavianLines)
print("done.")

#Replace words in en_us.lang (OptiFine)
print("Replacing words in OptiFine langfile...", end="", flush=True)
with open("latin/en_us.lang") as f:
	optifineLines = f.readlines()
shavianOFLines = []
for line in optifineLines:
	if line.startswith("#") or line == "\n":
		shavianOFLines.append(line)
		continue

	key = line.split("=", 1)[0]
	text = line.split("=", 1)[1][:-1].replace("\\n", "\n")

	newLine = "".join(replaceWords(text)).replace("\n", "\\n")
	
	shavianOFLines.append(f"{key}={newLine}\n")
print("done.")

print("Writing to shavian/new.lang...", end="", flush=True)
with open("shavian/new.lang", "w") as f:
	f.writelines(shavianOFLines)
print("done.")

#Write missing words to a file
print(f"Writing {len(missingWords)} missing words to wordlists/missingwords.tsv...", end="", flush=True)
with open("wordlists/missingwords.tsv", "w") as f:
	for word in missingWords:
		f.write(word + "\n")
print("done.")
